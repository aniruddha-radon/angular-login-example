const app = angular.module("loginApp", []);

app.controller("loginController", function($scope) {
    $scope.username = "";
    $scope.password = "";

    $scope.isSubmitted = false;

    $scope.login = function() {
        $scope.isSubmitted = true;
    }
})